﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace SharedCode.Localization
{
    /// <summary>
    /// Klasa serwisowa dla obsługi języka, w oparciu o wzorzec
    /// projektowy singleton
    /// </summary>
    public sealed class LanguageService
    {
        private static LanguageService _instance;
        private static readonly object _instanceLock = new object();
        private const int COOKIE_EXPIRATION_IN_DAYS = 30;

        /// <summary>
        /// Instancja/Konstruktor klasy
        /// </summary>
        public static LanguageService Instance
        {
            get
            {
                if (_instance == null)
                {
                    // Blokada wątku na _instanceLock - pozostałe wątki
                    // czekają do odblokowania tego obszaru
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                            _instance = new LanguageService();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Lista dostępnych języków
        /// </summary>
        public List<ILanguageModel> AvailableLanguages = new List<ILanguageModel>
        {
            new LanguageModel {
                LanguageName = "Polish", LanguageCultureName = "pl" // Default ResourceStrings.resx
            },
            new LanguageModel {
                LanguageName = "English", LanguageCultureName = "en"
            },
            new LanguageModel {
                LanguageName = "German", LanguageCultureName = "de"
            },
        };

        /// <summary>
        /// Sprawdzenie czy język jest dostępny
        /// </summary>
        /// <param name="languageName"></param>
        /// <returns></returns>
        public bool IsLanguageAvailable(string languageName)
        {
            return AvailableLanguages.Where(l => l.LanguageCultureName.Equals(languageName)).FirstOrDefault() != null ? true : false;
        }

        /// <summary>
        /// Język domyslny
        /// </summary>
        /// <returns></returns>
        public string GetDefaultLanguage()
        {
            return AvailableLanguages.FirstOrDefault().LanguageCultureName;
        }

        /// <summary>
        /// Metoda zwraca język używany w obrębie aktualnego wątku
        /// </summary>
        /// <returns></returns>
        public string GetCurrentLanguage()
        {
            var currentLanguage = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToString();

            if (string.IsNullOrEmpty(currentLanguage))
                return GetDefaultLanguage();
            else
                return currentLanguage;
        }

        /// <summary>
        /// Metoda ustawienia języka
        /// </summary>
        /// <param name="languageName"></param>
        public void SetLanguage(string languageName)
        {
            if (!IsLanguageAvailable(languageName))
                languageName = GetDefaultLanguage();
            var cultureInfo = new CultureInfo(languageName);

            SetLanguageInCurrentThread(cultureInfo);
            SetLanguageInCookies(languageName);
        }

        /// <summary>
        /// Ustawienie języka w ciasteczkach
        /// </summary>
        /// <param name="languageName"></param>
        private void SetLanguageInCookies(string languageName)
        {
            HttpCookie languageCookie = new HttpCookie(AppStrings.CookieCultureName, languageName);
            languageCookie.Expires = DateTime.Now.AddDays(COOKIE_EXPIRATION_IN_DAYS);
            HttpContext.Current.Response.Cookies.Add(languageCookie);
        }

        /// <summary>
        /// Ustawienie języka dla obecnego wątku
        /// </summary>
        /// <param name="cultureInfo"></param>
        private void SetLanguageInCurrentThread(CultureInfo cultureInfo)
        {
            if (cultureInfo != null)
            {
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
            }
        }
    }
}