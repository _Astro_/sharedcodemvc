﻿namespace SharedCode.Localization
{
    /// <summary>
    /// Interfejs klasy modelowej języka
    /// </summary>
    public interface ILanguageModel
    {
        string LanguageName { get; set; }

        string LanguageCultureName { get; set; }
    }
}