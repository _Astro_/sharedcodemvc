﻿namespace SharedCode.Localization
{
    /// <summary>
    /// Klasa modelowa języka dla lokalizacji
    /// </summary>
    public class LanguageModel : ILanguageModel
    {
        public string LanguageName { get; set; }

        public string LanguageCultureName { get; set; }
    }
}