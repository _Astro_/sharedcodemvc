﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using SharedCode.Identity.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SharedCode.Identity
{
    /// <summary>
    /// Klasa managera dla obsługi klasy użytkownika aplikacji w procesie logowania
    /// </summary>
    public class AppSignInManager : SignInManager<AppUserModel, string>
    {
        public AppSignInManager(AppUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(AppUserModel user)
        {
            return user.GenerateUserIdentityAsync((AppUserManager)UserManager);
        }

        public static AppSignInManager Create(IdentityFactoryOptions<AppSignInManager> options, IOwinContext context)
        {
            return new AppSignInManager(context.GetUserManager<AppUserManager>(), context.Authentication);
        }
    }
}