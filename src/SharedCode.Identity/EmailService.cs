﻿using Microsoft.AspNet.Identity;
using SharedCode.Localization;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SharedCode.Identity
{
    /// <summary>
    /// Klasa serwisowa dla obsługi adresu email skojarzonego z projektem
    /// </summary>
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            SmtpClient client = new SmtpClient();
            return client.SendMailAsync(AppStrings.ApplicationEmailAdres,
                                        message.Destination,
                                        message.Subject,
                                        message.Body);
        }
    }
}