﻿using Microsoft.AspNet.Identity.EntityFramework;
using SharedCode.Identity.Models;
using System.Data.Entity;

namespace SharedCode.Identity.DAL
{
    /// <summary>
    /// Klasa kontekstu bazy danych
    /// </summary>
    public partial class AppDbContext : IdentityDbContext<AppUserModel>
    {
        public AppDbContext()
            : base("DefaultConnection")
        {
            // Migracja bazy danych zgodnie z konfiguracją
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, SharedCode.Identity.Migrations.Configuration>());
        }

        /// <summary>
        /// Metoda zwraca nową instancję klasy
        /// </summary>
        /// <returns></returns>
        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }
}