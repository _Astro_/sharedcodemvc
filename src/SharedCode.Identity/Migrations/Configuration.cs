namespace SharedCode.Identity.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Konfiguracja migracji bazy danych
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<SharedCode.Identity.DAL.AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "SharedCode.Identity.DAL.AppDbContext";
        }

        protected override void Seed(SharedCode.Identity.DAL.AppDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}