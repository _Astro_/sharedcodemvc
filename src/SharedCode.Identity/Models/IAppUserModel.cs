﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SharedCode.Identity.Models
{
    /// <summary>
    /// Interfejs klasy modelowej użytkownika aplikacji
    /// </summary>
    public interface IAppUserModel
    {
        string Email { get; set; }

        string UserName { get; set; }

        string PasswordHash { get; set; }

        string SecurityStamp { get; set; }

        Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUserModel> manager);
    }
}