﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharedCode.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SharedCode.Identity.Models
{
    /// <summary>
    /// Klasa modelowa użytkownika aplikacji
    /// </summary>
    public class AppUserModel : IdentityUser, IAppUserModel
    {
        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "AppUserModelEmptyEmail")]
        [EmailAddress(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "AppUserModelInvalidEmail")]
        public override string Email { get => base.Email; set => base.Email = value; }

        public override string UserName
        {
            get
            {
                if (string.IsNullOrEmpty(base.UserName))
                    return base.Email;
                else
                    return base.UserName;
            }
            set => base.UserName = value;
        }

        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "AppUserModelEmptyPassword")]
        [DataType(DataType.Password)]
        public override string PasswordHash { get => base.PasswordHash; set => base.PasswordHash = value; }

        public override string SecurityStamp
        {
            get
            {
                if (string.IsNullOrEmpty(base.SecurityStamp))
                    return DateTime.Now.ToLongDateString();
                else
                    return base.SecurityStamp;
            }

            set => base.SecurityStamp = value;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUserModel> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            return userIdentity;
        }
    }
}