﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SharedCode.Identity.DAL;
using SharedCode.Identity.Models;

namespace SharedCode.Identity
{
    /// <summary>
    /// Klasa managera dla obsługi klasy użytkownika aplikacji w procesie autoryzacji
    /// </summary>
    public class AppUserManager : UserManager<AppUserModel>
    {
        public AppUserManager(IUserStore<AppUserModel> store) : base(store)
        {
        }

        /// <summary>
        /// Statyczna metoda konstrukcyjna wraz z konfiguracją
        /// </summary>
        /// <param name="options"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var userManager = new AppUserManager(new UserStore<AppUserModel>(context.Get<AppDbContext>()));

            // Walidacje dla parametrów użytkownika
            userManager.UserValidator = new UserValidator<AppUserModel>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false
            };

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                userManager.UserTokenProvider =
                    new DataProtectorTokenProvider<AppUserModel>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            userManager.EmailService = new EmailService();

            return userManager;
        }
    }
}