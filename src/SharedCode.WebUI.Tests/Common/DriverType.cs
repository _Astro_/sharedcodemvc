﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedCode.WebUI.Tests.Common
{
    /// <summary>
    /// Klasa wyliczeniowa dla typów sterowników pzreglądarek
    /// </summary>
    public enum DriverType
    {
        Chrome,
        Firefox,
        Edge
    }
}
