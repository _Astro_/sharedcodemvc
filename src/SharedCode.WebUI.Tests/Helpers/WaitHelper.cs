﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedCode.WebUI.Tests.Helpers
{
    public class WaitHelper
    {
        private IWebDriver _webDriver;

        public WaitHelper(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public WebDriverWait Wait()
        {
            var wait = new WebDriverWait(_webDriver, TimeOutHelper.TimeOutInSec);
            return wait;
        }

        public void WaitForClickable(IWebElement webElement)
        {
            Wait().Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(webElement));
        }
    }
}
