﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using SharedCode.WebUI.Tests.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedCode.WebUI.Tests.Drivers
{
    /// <summary>
    /// Fakryka sterówników przeglądarek Web
    /// </summary>
    public static class DriverFactory
    {
        /// <summary>
        /// Metoda zwraca nową instancję sterownika pzreglądarki Web
        /// </summary>
        /// <param name="driverType">Typ sterownika</param>
        /// <returns></returns>
        public static IWebDriver GetDriver(DriverType driverType)
        {
            switch (driverType)
            {
                case DriverType.Chrome:
                    return new ChromeDriver();
                case DriverType.Firefox:
                    return new FirefoxDriver();
                case DriverType.Edge:
                    return new EdgeDriver();
                default:
                    return null;
            }
        }
    }
}
