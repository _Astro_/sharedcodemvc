﻿Feature: SharedCodeWebUILoginPage

@mytag
Scenario: Login with unregistered credentials
	Given I'm in the login page
	When Fill unregistered credentials
	Then Page return error message about invalid credentials

Scenario: Login with invalid email
	Given I'm in the login page
	When Fill invalid email
	Then Page return error message about invalid email