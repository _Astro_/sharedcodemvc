﻿using BoDi;
using OpenQA.Selenium;
using SharedCode.WebUI.Tests.Common;
using SharedCode.WebUI.Tests.Drivers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SharedCode.WebUI.Tests.Hooks
{
    /// <summary>
    /// Klasa ustawień dla testów
    /// </summary>
    [Binding]
    public sealed class LoginPageHooks
    {
        private IWebDriver _webDriver;
        private readonly IObjectContainer _objectContainer;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="objectContainer">DI kontenera obiektów</param>
        public LoginPageHooks (IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        /// <summary>
        /// Wywoływane przed każdym scenariuszem
        /// </summary>
        [BeforeScenario]
        public void BeforeScenario()
        {
            _webDriver = DriverFactory.GetDriver(DriverType.Chrome);
            _objectContainer.RegisterInstanceAs<IWebDriver>(_webDriver);
        }

        /// <summary>
        /// Wywoływane po każdym scenariuszu
        /// </summary>
        [AfterScenario]
        public void AfterScenario()
        {
            if (_webDriver != null)
            {
                _webDriver.Close();
                _webDriver.Dispose();
            }
        }
    }
}
