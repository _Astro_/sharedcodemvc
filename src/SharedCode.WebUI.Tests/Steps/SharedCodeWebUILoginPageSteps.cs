﻿using NUnit.Framework;
using OpenQA.Selenium;
using SharedCode.WebUI.Tests.Helpers;
using System;
using TechTalk.SpecFlow;

namespace SharedCode.WebUI.Tests.Steps
{
    /// <summary>
    /// Klasa definicji kroków testowanego scenariusza
    /// </summary>
    [Binding]
    public class SharedCodeWebUILoginPageSteps
    {
        private IWebDriver _webDriver;
        private WaitHelper _waitHelper;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="webDriver">DI sterownik przeglądarki Web</param>
        public SharedCodeWebUILoginPageSteps(IWebDriver webDriver)
        {
            _webDriver = webDriver;
            _waitHelper = new WaitHelper(_webDriver);
        }

        [Given(@"I'm in the login page")]
        public void GivenIMInTheLoginPage()
        {
            _webDriver.Navigate().GoToUrl("http://localhost:8083/"); // Pod tym adresem publikuję lokalnie stronę
        }
        
        [When(@"Fill unregistered credentials")]
        public void WhenFillUnregisteredCredentials()
        {
            var email = _webDriver.FindElement(By.Id("Email"));
            var password = _webDriver.FindElement(By.Id("PasswordHash"));
            var submit = _webDriver.FindElement(By.Id("LoginSubbmitButton"));

            email.SendKeys("unregisteredEmail@gmail.com");
            password.SendKeys("anyPassword");

            _waitHelper.WaitForClickable(submit);
            submit.Click();
        }

        [Then(@"Page return error message about invalid credentials")]
        public void ThenPageReturnErrorMessageAboutInvalidCredentials()
        {
            var errorLabel = _webDriver.FindElement(By.Id("ErrorLabel"));
            var errorMessage = errorLabel.Text;

            Assert.IsTrue(!string.IsNullOrEmpty(errorMessage));
        }

        [When(@"Fill invalid email")]
        public void WhenFillInvalidEmail()
        {
            var email = _webDriver.FindElement(By.Id("Email"));
            var password = _webDriver.FindElement(By.Id("PasswordHash"));
            var submit = _webDriver.FindElement(By.Id("LoginSubbmitButton"));

            email.SendKeys("Invalid email address");
            password.SendKeys("anyPassword");

            _waitHelper.WaitForClickable(submit);
            submit.Click();
        }

        [Then(@"Page return error message about invalid email")]
        public void ThenPageReturnErrorMessageAboutInvalidEmail()
        {
            var errorLabel = _webDriver.FindElement(By.Id("EmailValidationMessage"));
            var errorMessage = errorLabel.Text;

            Assert.IsTrue(!string.IsNullOrEmpty(errorMessage));
        }

    }
}
