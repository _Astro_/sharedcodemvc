﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SharedCode.WebUI.Startup))]

namespace SharedCode.WebUI
{
    /// <summary>
    /// Klasa bootstrap'u dla OWIN z konfiguracją
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Konfiguracja właściwa przeniesiona do StartupConfig w folderze AppStart
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            Configure(app);
        }
    }
}
