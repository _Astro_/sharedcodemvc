﻿using AutoMapper;
using SharedCode.Code.Models;
using SharedCode.WebUI.Models;

namespace SharedCode.WebUI.Maps
{
    /// <summary>
    /// Klasa domyślnego profilu dla automappera
    /// </summary>
    public class AppProfile : Profile
    {
        /// <summary>
        /// Konstruktor i mapy klas/obiektów
        /// </summary>
        public AppProfile()
        {
            CreateMap<ProjectRoomModel, ProjectRoomViewModel>();
            CreateMap<ProjectRoomViewModel, ProjectRoomModel>();
        }
    }
}