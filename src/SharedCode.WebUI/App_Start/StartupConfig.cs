﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using SharedCode.Identity;
using SharedCode.Identity.DAL;
using SharedCode.Identity.Models;
using SharedCode.Localization;
using System;

namespace SharedCode.WebUI
{
    /// <summary>
    /// Klasa bootstrap'u dla OWIN oraz SignalR
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Konfiguracja OWIN oraz SignalR dla aplikacji
        /// </summary>
        /// <param name="app"></param>
        public void Configure(IAppBuilder app)
        {
            IdentityConfig(app);
            SignalRConfig(app);
        }

        private void IdentityConfig(IAppBuilder app)
        {
            // Tworzy nowy obiekt dla każdego żądania
            app.CreatePerOwinContext(AppDbContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<AppSignInManager>(AppSignInManager.Create);

            // Parametry autentyfikacji poprzez cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString(string.Format("/{0}/{1}", AppStrings.LoginControllerName, AppStrings.LoginActionName)),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUserModel>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
        }

        private void SignalRConfig(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}