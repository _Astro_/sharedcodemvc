﻿using System.Web.Mvc;

namespace SharedCode.WebUI
{
    /// <summary>
    /// Klasa konfiguracji filtrów
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Globalna rejestracja filtrów
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeAttribute()); // Domyślnie wszystkie kontrolery/akcje wymagają autoryzacji
        }
    }
}