﻿using SharedCode.Localization;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SharedCode.WebUI.Controllers
{
    /// <summary>
    /// Abstrakcyjna klasa bazowa dla wszystkich klas typu 'controller'
    /// </summary>
    [Authorize]
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Metoda zmiany języka
        /// </summary>
        /// <param name="languageName"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ChangeLanguage(string languageName)
        {
            LanguageService.Instance.SetLanguage(languageName);

            return Redirect(Request.UrlReferrer.ToString());
        }

        /// <summary>
        /// Obsługa zmiany języka
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string language = string.Empty;

            HttpCookie languageCookie = Request.Cookies[AppStrings.CookieCultureName];

            if (languageCookie != null)
            {
                language = languageCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage.FirstOrDefault() : string.Empty;

                if (!string.IsNullOrEmpty(userLang))
                {
                    language = userLang;
                }
                else
                {
                    language = LanguageService.Instance.GetDefaultLanguage();
                }
            }
            LanguageService.Instance.SetLanguage(language);

            return base.BeginExecuteCore(callback, state);
        }

        /// <summary>
        /// Obsługa błędów grubych aplikacji
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            filterContext.ExceptionHandled = true;

            string controllerName = (string)filterContext.RouteData.Values["controller"];
            string actionName = (string)filterContext.RouteData.Values["action"];
            var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error.cshtml",
                ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                TempData = filterContext.Controller.TempData
            };
        }
    }
}