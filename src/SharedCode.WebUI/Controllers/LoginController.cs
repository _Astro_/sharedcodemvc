﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SharedCode.Identity;
using SharedCode.Identity.Models;
using SharedCode.Localization;
using SharedCode.WebUI.Models;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SharedCode.WebUI.Controllers
{
    /// <summary>
    /// Kontroler strony logowania do aplikacji
    /// </summary>
    public class LoginController : BaseController
    {
        private AppUserManager _userManager;
        private AppSignInManager _signInManager;

        public AppUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        public AppSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<AppSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View(AppStrings.LoginPageName);
        }

        /// <summary>
        /// Metoda logowania użytkownika
        /// </summary>
        /// <param name="appUserModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AppUserModel appUserModel)
        {
            // Niewypełnion pola
            if (!ModelState.IsValid)
            {
                return View(AppStrings.LoginPageName);
            }

            // Sprawdzanie czy konto zostało aktywowane
            var user = await UserManager.FindByNameAsync(appUserModel.Email);
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    TempData[AppStrings.ErrorMessageTag] = LanguageStrings.InactiveAccount;
                    return View(AppStrings.LoginPageName);
                }
            }

            var result = await SignInManager.PasswordSignInAsync(appUserModel.Email, appUserModel.PasswordHash, isPersistent: false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction(AppStrings.HomePageName, AppStrings.HomeControllerName);
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    TempData[AppStrings.ErrorMessageTag] = LanguageStrings.InactiveAccount;
                    return View(AppStrings.LoginPageName);

                case SignInStatus.Failure:
                    TempData[AppStrings.ErrorMessageTag] = LanguageStrings.AppUserModelNoModel;
                    return View(AppStrings.LoginPageName);

                default:
                    TempData[AppStrings.ErrorMessageTag] = LanguageStrings.AppUserModelNoModel;
                    return View(AppStrings.LoginPageName);
            }
        }

        /// <summary>
        /// Obsługa wylogowania
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Logout()
        {
            GetAuthenticationManager().SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction(AppStrings.LoginActionName, AppStrings.LoginControllerName);
        }

        /// <summary>
        /// rejestracja - metoda GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(AppStrings.RegisterPageName);
        }

        /// <summary>
        /// Rejestracja - metoda POST
        /// </summary>
        /// <param name="appUserModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(AppUserModel appUserModel)
        {
            if (!ModelState.IsValid)
            {
                return View(AppStrings.RegisterPageName);
            }

            var user = new AppUserModel { UserName = appUserModel.Email, Email = appUserModel.Email };
            var result = await UserManager.CreateAsync(user, appUserModel.PasswordHash);

            if (result.Succeeded)
            {
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action(
                    controllerName: AppStrings.LoginControllerName,
                    actionName: AppStrings.ConfirmAccountActionName,
                    routeValues: new { userId = user.Id, code },
                    protocol: Request.Url.Scheme);

                await UserManager.SendEmailAsync(
                    user.Id,
                    LanguageStrings.ConfirmAccountEmailTitle,
                    string.Format("{0}\n{1}", LanguageStrings.ConfirmAccountEmailContent, callbackUrl));

                TempData[AppStrings.ErrorMessageTag] = LanguageStrings.ConfirmAccountEmailSended;

                return View(AppStrings.RegisterPageName);
            }

            foreach (var error in result.Errors)
            {
                TempData[AppStrings.ErrorMessageTag] = string.Format("{0}\n{1}", TempData[AppStrings.ErrorMessageTag], error);
            }

            return View(AppStrings.RegisterPageName);
        }

        /// <summary>
        /// Potwierdzenie/aktywacja konta
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmAccount(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View(AppStrings.LoginPageName);
            }

            var result = await UserManager.ConfirmEmailAsync(userId, code);

            return View(result.Succeeded ? AppStrings.ConfirmAccountPageName : AppStrings.LoginPageName);
        }

        /// <summary>
        /// Strona przypomnienia hasła - metoda GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View(AppStrings.ForgotPasswordPageTitle);
        }

        /// <summary>
        /// Strona przypomnienia hasła - metoda POST
        /// </summary>
        /// <param name="forgotPasswordViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel forgotPasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(forgotPasswordViewModel.Email);

                if (user == null)
                {
                    TempData[AppStrings.EmailResetConfirmationMessageTag] = LanguageStrings.ForgotPasswordResetLinkSended;
                    return View(AppStrings.ForgotPasswordPageTitle);
                }
                else
                {
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                    var callbackUrl = Url.Action(
                        controllerName: AppStrings.LoginControllerName,
                        actionName: AppStrings.ResetPasswordAction,
                        routeValues: new { userId = user.Id, code },
                        protocol: Request.Url.Scheme);

                    await UserManager.SendEmailAsync(
                        user.Id,
                        LanguageStrings.ResetPasswordEmailTitle,
                        string.Format("{0}\n{1}", LanguageStrings.ResetPasswordEmailContent, callbackUrl));

                    TempData[AppStrings.EmailResetConfirmationMessageTag] = LanguageStrings.ForgotPasswordResetLinkSended;
                    return View(AppStrings.ForgotPasswordPageTitle);
                }
            }

            return View(AppStrings.ForgotPasswordPageTitle);
        }

        /// <summary>
        /// Strona resetowania hasła - metoda GET
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string code)
        {
            return code == null ? View(AppStrings.LoginPageName) : View(AppStrings.ResetPasswordPageName);
        }

        /// <summary>
        /// Strona resetowania hasła - metoda POST
        /// </summary>
        /// <param name="resetPasswordViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(resetPasswordViewModel.UserId);
                if (user == null)
                {
                    return View(AppStrings.ResetPasswordPageName);
                }

                var result = await UserManager.ResetPasswordAsync(user.Id, resetPasswordViewModel.Code, resetPasswordViewModel.Password);
                if (result.Succeeded)
                {
                    TempData[AppStrings.ResetPasswordMessageTag] = LanguageStrings.ResetPasswordSuccess;
                    return View(AppStrings.ResetPasswordPageName);
                }
            }

            return View(AppStrings.ResetPasswordPageName);
        }

        /// <summary>
        /// Metoda uwierzytelnienia użytkownika
        /// </summary>
        /// <param name="appUserModel"></param>
        /// <returns></returns>
        private async Task SignIn(AppUserModel appUserModel)
        {
            var identity = await UserManager.CreateIdentityAsync(appUserModel, DefaultAuthenticationTypes.ApplicationCookie);
            GetAuthenticationManager().SignIn(identity);
        }

        /// <summary>
        /// Metoda zwraca managera uwierzytelnienia
        /// </summary>
        /// <returns></returns>
        private IAuthenticationManager GetAuthenticationManager()
        {
            return HttpContext.GetOwinContext().Authentication;
        }

        /// <summary>
        /// Razem z klasą zwalniany jest manager użytkowników, w tym DbContext
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            if (disposing && _signInManager != null)
            {
                _signInManager.Dispose();
                _signInManager = null;
            }

            base.Dispose(disposing);
        }
    }
}