﻿using AutoMapper;
using SharedCode.Code.DAL;
using SharedCode.Code.Models;
using SharedCode.Localization;
using SharedCode.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SharedCode.WebUI.Controllers
{
    /// <summary>
    /// Kontroler strony domowej aplikacji
    /// </summary>
    public class HomeController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private ProjectRoomPageViewModel _projectRoomPageViewModel;

        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        /// <param name="mapper"></param>
        public HomeController(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ProjectRoomPageViewModel ProjectRoomPageViewModel
        {
            get
            {
                if (_projectRoomPageViewModel == null)
                {
                    _projectRoomPageViewModel = new ProjectRoomPageViewModel();
                    _projectRoomPageViewModel.AddProjectRoomViewModel = new ProjectRoomViewModel();
                }                    
                return _projectRoomPageViewModel;
            }
            set
            {
                _projectRoomPageViewModel = value;
            }
        }

        /// <summary>
        /// Strona domowa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ViewResult> Index()
        {
            ProjectRoomPageViewModel.ProjectRoomViewModels = await GetProjectRoomViewModelList();

            return View(ProjectRoomPageViewModel);
        }

        /// <summary>
        /// Dodawanie nowego projektu
        /// </summary>
        /// <param name="projectRoomPageViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(ProjectRoomPageViewModel projectRoomPageViewModel)
        {
            if (ModelState.IsValid)
            {
                var projectRoomViewModel = projectRoomPageViewModel.AddProjectRoomViewModel;

                if (await CheckIfExists(projectRoomViewModel) == false)
                {
                    if (projectRoomViewModel.Access == LanguageStrings.AccessPublic)
                        await SavePublicProjectRoom(projectRoomViewModel);
                    else
                        await SavePrivateProjectRoom(projectRoomViewModel);
                }
                else
                {
                    TempData[AppStrings.ErrorMessageTag] = LanguageStrings.ProjectRoomModelAlreadyExists;
                }
            }

            projectRoomPageViewModel.ProjectRoomViewModels = await GetProjectRoomViewModelList();

            return View(projectRoomPageViewModel);
        }

        /// <summary>
        /// Logowanie do strony projektu
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Access"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ProjectRoomLogin(string Id,  string Access, string Password)
        {
            if (Access == LanguageStrings.AccessPublic)
                return RedirectToAction(controllerName: AppStrings.CodeControllerName, actionName: AppStrings.CodeActionName, routeValues: new { Id });
            else
                return await LoginToPrivateRoom(Id, Password);           
        }

        /// <summary>
        /// Strona 'O programie'
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult About()
        {
            return View(AppStrings.AboutPageName);
        }

        /// <summary>
        /// Strona 'Polityka prywatności'
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Privacy()
        {
            return View(AppStrings.PrivacyPageName);
        }


        /// <summary>
        /// Metoda sprawdza czy string wejściowy to string dostępu publicznego
        /// </summary>
        /// <param name="accessString"></param>
        /// <returns></returns>
        [HttpGet]
        public string CheckIsPublicAccess(string accessString)
        {
            return string.Equals(accessString, LanguageStrings.AccessPublic).ToString();
        }

        /// <summary>
        /// Metoda logowania do prywatnego projektu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private async Task<ActionResult> LoginToPrivateRoom(string id, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                TempData[AppStrings.ErrorMessageTag + id] = LanguageStrings.ProjectRoomModelEmptyPassword;
                return RedirectToAction(controllerName: AppStrings.HomeControllerName, actionName: AppStrings.IndexActionName);
            }

            var projectRoomModel = await GetProjectRoomByExpressionAsync(m => m.Id == id && m.Password == password);

            if (projectRoomModel != null)
                return RedirectToAction(controllerName: AppStrings.CodeControllerName, actionName: AppStrings.CodeActionName, routeValues: new { id });
            else
            {
                TempData[AppStrings.ErrorMessageTag + id] = LanguageStrings.ProjectRoomModelIncorrectPassword;
                return RedirectToAction(controllerName: AppStrings.HomeControllerName, actionName: AppStrings.IndexActionName);
            }
        }

        /// <summary>
        /// Metoda zapisu prywatnego modelu projektu pokoju
        /// </summary>
        /// <param name="projectRoomViewModel"></param>
        /// <returns></returns>
        private async Task SavePrivateProjectRoom(ProjectRoomViewModel projectRoomViewModel)
        {
            if (string.IsNullOrEmpty(projectRoomViewModel.Password))
            {
                TempData[AppStrings.ErrorMessageTag] = LanguageStrings.ProjectRoomModelEmptyPassword;
            }
            else
            {
                await SaveProjectRoom(projectRoomViewModel, AppStrings.PrivateString);
            }
        }

        /// <summary>
        /// Metoda zapisu publicznego modelu projektu pokoju
        /// </summary>
        /// <param name="projectRoomViewModel"></param>
        /// <returns></returns>
        private async Task SavePublicProjectRoom(ProjectRoomViewModel projectRoomViewModel)
        {
            await SaveProjectRoom(projectRoomViewModel, AppStrings.PublicString);
        }

        /// <summary>
        /// Metoda zapisu modelu projektu pokoju
        /// </summary>
        /// <param name="projectRoomViewModel"></param>
        /// <param name="access"></param>
        /// <returns></returns>
        private async Task SaveProjectRoom(ProjectRoomViewModel projectRoomViewModel, string access)
        {
            try
            {
                _unitOfWork = GetUnitOfWorkInstance();
                var projectRoomModel = _mapper.Map<ProjectRoomViewModel, ProjectRoomModel>(projectRoomViewModel);
                projectRoomModel.Access = access;
                _unitOfWork.ProjectRoomRepository.AddAsync(projectRoomModel);

                await _unitOfWork.Commit();
            }
            catch (Exception exception)
            {
                TempData[AppStrings.ErrorMessageTag] = exception.ToString();
            }
            finally
            {
                _unitOfWork.Dispose();
            }
        }


        /// <summary>
        /// Metoda zwraca aktualną listę klas typu ProjectRoomViewModel
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<ProjectRoomViewModel>> GetProjectRoomViewModelList()
        {
            _unitOfWork = GetUnitOfWorkInstance();
            var projectRoomModelEnum = await _unitOfWork.ProjectRoomRepository.GetAllAsync();

            var projectRoomViewModelEnum = _mapper.Map<IEnumerable<ProjectRoomModel>, IEnumerable<ProjectRoomViewModel>>(projectRoomModelEnum);
            _unitOfWork.Dispose();

            return projectRoomViewModelEnum;
        }

        /// <summary>
        /// Metoda sprawdza czy model o danej nazwie już istnieje
        /// </summary>
        /// <param name="projectRoomViewModel"></param>
        /// <returns></returns>
        private async Task<bool> CheckIfExists(IProjectRoomViewModel projectRoomViewModel)
        {
            _unitOfWork = GetUnitOfWorkInstance();
            var model = await _unitOfWork.ProjectRoomRepository.GetByExpressionsAsync(
                m => m.Name == projectRoomViewModel.Name);
            _unitOfWork.Dispose();

            return model.FirstOrDefault() != null;
        }

        /// <summary>
        /// Metoda zwraca obiekt bazując na wyrażeniu LINQ
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        private async Task<IProjectRoomModel> GetProjectRoomByExpressionAsync(Expression<Func<ProjectRoomModel,bool>> predicate)
        {
            _unitOfWork = GetUnitOfWorkInstance();
            var projectRoomModel = await _unitOfWork.ProjectRoomRepository.GetByExpressionsAsync(predicate);
            _unitOfWork.Dispose();

            return projectRoomModel.FirstOrDefault();
        }

        /// <summary>
        /// Metoda zwraca nową instancję klasy UnitOfWork
        /// </summary>
        /// <returns></returns>
        private IUnitOfWork GetUnitOfWorkInstance()
        {
            if (_unitOfWork != null)
            {
                _unitOfWork.Dispose();
                _unitOfWork = null;
            }

            return new UnitOfWork();
        }
    }
}