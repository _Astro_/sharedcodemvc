﻿using AutoMapper;
using SharedCode.Code.DAL;
using SharedCode.Code.Models;
using SharedCode.WebUI.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SharedCode.WebUI.Controllers
{
    public class CodeController : BaseController
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        /// <param name="mapper"></param>
        public CodeController(IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// Strona główna
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> CodeRoom(string Id)
        {
            var projectRoomViewModel = await GetProjectRoomViewModel(Id);
            return View(projectRoomViewModel);
        }

        /// <summary>
        /// Metoda zwraca aktualną klasę typu ProjectRoomViewModel
        /// </summary>
        /// <returns></returns>
        private async Task<ProjectRoomViewModel> GetProjectRoomViewModel(string id)
        {
            _unitOfWork = GetUnitOfWorkInstance();
            var projectRoomModel = await _unitOfWork.ProjectRoomRepository.GetByIdAsync(id);

            var projectRoomViewModel = _mapper.Map<ProjectRoomModel, ProjectRoomViewModel>(projectRoomModel);
            _unitOfWork.Dispose();

            return projectRoomViewModel;
        }

        /// <summary>
        /// Metoda zwraca nową instancję klasy UnitOfWork
        /// </summary>
        /// <returns></returns>
        private IUnitOfWork GetUnitOfWorkInstance()
        {
            if (_unitOfWork != null)
            {
                _unitOfWork.Dispose();
                _unitOfWork = null;
            }

            return new UnitOfWork();
        }
    }
}