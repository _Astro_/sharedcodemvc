﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SharedCode.WebUI.Hubs
{
    public class AppUsersHub : Hub
    {
        private static List<string> _onlineUsersList = new List<string>();
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        private int _currentCount = 0;
        // Satyaprakash MVC signalR Crud Using EF 6
        public override Task OnConnected()
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                _connections.Add(Context.User.Identity.Name, Context.ConnectionId);
                Notify();
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                _connections.Remove(Context.User.Identity.Name, Context.ConnectionId);
                Notify();
            }

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                _connections.Add(Context.User.Identity.Name, Context.ConnectionId);
                Notify();
            }

            return base.OnReconnected();
        }

        private void Notify()
        {
            if (IsCountChange())
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<AppUsersHub>();
                context.Clients.All.UsersOnlineList(_connections);
                context.Clients.All.UsersOnlineCount(_connections.Count);
            }            
        }

        private bool IsCountChange()
        {
            if (_connections.Count != _currentCount)
            {
                _currentCount = _connections.Count;
                return true;
            }
            return false;
        }

    }
}