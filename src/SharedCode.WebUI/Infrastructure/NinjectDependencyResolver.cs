﻿using AutoMapper;
using Ninject;
using Ninject.Activation;
using SharedCode.WebUI.Maps;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SharedCode.WebUI.Infrastructure
{
    /// <summary>
    /// Klasa resolvera dla DI
    /// </summary>
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        /// <summary>
        /// Rejestracja modułów i serwisów
        /// </summary>
        private void AddBindings()
        {
            kernel.Bind<IMapper>().ToMethod(AppAutoMapper).InSingletonScope();
        }

        /// <summary>
        /// Metoda konfiguracji automappera
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IMapper AppAutoMapper(IContext context)
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AppProfile());
                cfg.ConstructServicesUsing(type => context.Kernel.Get(type));
            });

            mapperConfiguration.AssertConfigurationIsValid();
            var mapper = mapperConfiguration.CreateMapper();            

            return mapper;
        }
    }
}