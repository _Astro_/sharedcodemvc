﻿namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Interfejs klasy widoku modelu dla przypomnienia hasła
    /// </summary>
    public interface IForgotPasswordViewModel
    {
        string Email { get; set; }
    }
}