﻿using SharedCode.Localization;
using System.ComponentModel.DataAnnotations;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Klasa widoku modelu dla ekranu resetowania hasła
    /// </summary>
    public class ResetPasswordViewModel : IResetPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "ResetPasswordViewModelEmptyPassword")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "ResetPasswordViewModelEmptyPasswordConfirmation")]
        [Compare("Password", ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "PasswordAndConfirmationNotMatch")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public string UserId { get; set; }

        public string Code { get; set; }
    }
}