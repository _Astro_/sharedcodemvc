﻿using System.Collections.Generic;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Klasa widoku modelu dla strony z obsługą pokoi projektowych
    /// </summary>
    public class ProjectRoomPageViewModel : IProjectRoomPageViewModel
    {
        public ProjectRoomViewModel AddProjectRoomViewModel { get; set; }

        public IEnumerable<IProjectRoomViewModel> ProjectRoomViewModels { get; set; }
    }
}