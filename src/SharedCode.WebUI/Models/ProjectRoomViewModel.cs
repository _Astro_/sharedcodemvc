﻿using Microsoft.AspNet.Identity;
using SharedCode.Code.Models;
using SharedCode.Localization;
using System;
using System.Web;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Klasa widoku modelu dla pokoju projektu
    /// </summary>
    public class ProjectRoomViewModel : ProjectRoomModel, IProjectRoomViewModel
    {
        /// <summary>
        /// Nowe Id dla nowego obiektu
        /// </summary>
        public override string Id
        {
            get
            {
                if (string.IsNullOrEmpty(base.Id))
                    base.Id = Guid.NewGuid().ToString();
                return base.Id;
            }
            set => base.Id = value;
        }

        /// <summary>
        /// Lokalizacja pola Access
        /// </summary>
        public override string Access
        {
            get
            {
                if (string.IsNullOrEmpty(base.Access))
                    return LanguageStrings.AccessPublic;
                if (base.Access == AppStrings.PublicString)
                    return LanguageStrings.AccessPublic;
                else if (base.Access == AppStrings.PrivateString)
                    return LanguageStrings.AccessPrivate;
                else
                    return base.Access;
            }
            set
            {
                if (value == LanguageStrings.AccessPublic)
                    base.Access = AppStrings.PublicString;
                else if (value == LanguageStrings.AccessPrivate)
                    base.Access = AppStrings.PrivateString;
                else
                    base.Access = value;
            }
        }

        /// <summary>
        /// Data ostatniej aktywności
        /// </summary>
        public override DateTime? LastActive
        {
            get
            {
                if (base.LastActive == null)
                    base.LastActive = DateTime.Now;
                return base.LastActive;
            }
            set => base.LastActive = value;
        }

        /// <summary>
        /// Przypisanie ID obecnie zalogowanego użytkownika
        /// </summary>
        public override string CreatedById
        {
            get
            {
                if (string.IsNullOrEmpty(base.CreatedById))
                {
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        base.CreatedById = HttpContext.Current.User.Identity.GetUserId();
                    }
                }                    
                return base.CreatedById;
            }
            set => base.CreatedById = value;
        }

        /// <summary>
        /// Projekt domyslnie aktywny
        /// </summary>
        public override bool? IsActive
        {
            get
            {
                if (base.IsActive == null)
                    base.IsActive = true;
                return base.IsActive;
            }
            set => base.IsActive = value;
        }
    }
}