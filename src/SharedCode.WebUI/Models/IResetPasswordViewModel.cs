﻿namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Interfejs klasy widoku modelu dla ekranu resetowania hasła
    /// </summary>
    public interface IResetPasswordViewModel
    {
        string Password { get; set; }

        string ConfirmPassword { get; set; }

        string UserId { get; set; }

        string Code { get; set; }
    }
}