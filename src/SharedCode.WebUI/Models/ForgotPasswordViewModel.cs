﻿using SharedCode.Localization;
using System.ComponentModel.DataAnnotations;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Klasa widoku modelu dla przypomnienia hasła
    /// </summary>
    public class ForgotPasswordViewModel : IForgotPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "AppUserModelEmptyEmail")]
        [EmailAddress(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "AppUserModelInvalidEmail")]
        public string Email { get; set; }
    }
}