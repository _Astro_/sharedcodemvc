﻿using System;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Interfejs klasy widoku modelu dla pokoju projektu
    /// </summary>
    public interface IProjectRoomViewModel
    {
        string Id { get; set; }

        string Name { get; set; }

        string Access { get; set; }

        string Password { get; set; }

        string CreatedById { get; set; }

        bool? IsActive { get; set; }

        DateTime? LastActive { get; set; }
    }
}