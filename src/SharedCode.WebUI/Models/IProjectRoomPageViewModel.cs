﻿using System.Collections.Generic;

namespace SharedCode.WebUI.Models
{
    /// <summary>
    /// Klasa interfejsu widoku modelu dla strony z obsługą pokoi projektowych
    /// </summary>
    public interface IProjectRoomPageViewModel
    {
        ProjectRoomViewModel AddProjectRoomViewModel { get; set; }

        IEnumerable<IProjectRoomViewModel> ProjectRoomViewModels { get; set; }
    }
}