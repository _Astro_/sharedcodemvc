﻿using SharedCode.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SharedCode.Code.Models
{
    /// <summary>
    /// Klasa modelu pokoju dla projektu
    /// </summary>
    [Table("AspNetProjectRooms")]
    public class ProjectRoomModel : IProjectRoomModel
    {
        [Required]
        public virtual string Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(LanguageStrings), ErrorMessageResourceName = "ProjectRoomModelEmptyName")]
        public virtual string Name { get; set; }

        [Required]
        public virtual string Access { get; set; }

        [DataType(DataType.Password)]
        public virtual string Password { get; set; }

        [Required]
        public virtual string CreatedById { get; set; }

        public virtual bool? IsActive { get; set; }

        public virtual DateTime? LastActive { get; set; }
    }
}