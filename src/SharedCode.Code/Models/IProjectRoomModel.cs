﻿using System;

namespace SharedCode.Code.Models
{
    /// <summary>
    /// Interfejs klasy modelu pokoju dla projektu
    /// </summary>
    public interface IProjectRoomModel
    {
        string Id { get; set; }

        string Name { get; set; }

        string Access { get; set; }

        string Password { get; set; }

        string CreatedById { get; set; }

        bool? IsActive { get; set; }

        DateTime? LastActive { get; set; }
    }
}