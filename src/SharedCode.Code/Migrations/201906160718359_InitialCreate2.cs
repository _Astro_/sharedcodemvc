namespace SharedCode.Code.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetProjectRooms", "LastActive", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetProjectRooms", "LastActive");
        }
    }
}
