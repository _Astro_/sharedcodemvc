namespace SharedCode.Code.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetProjectRooms", "CreatedById", c => c.String(nullable: false));
            AddColumn("dbo.AspNetProjectRooms", "IsActive", c => c.Boolean());
            AlterColumn("dbo.AspNetProjectRooms", "Access", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetProjectRooms", "Access", c => c.String());
            DropColumn("dbo.AspNetProjectRooms", "IsActive");
            DropColumn("dbo.AspNetProjectRooms", "CreatedById");
        }
    }
}
