﻿using SharedCode.Code.Models;
using System.Data.Entity;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Klasa kontekstu bazy danych
    /// </summary>
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base("DefaultConnection")
        {
            // Migracja bazy danych zgodnie z konfiguracją
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, SharedCode.Code.Migrations.Configuration>());
        }

        public virtual DbSet<ProjectRoomModel> ProjectRoomModels { get; set; }
    }
}