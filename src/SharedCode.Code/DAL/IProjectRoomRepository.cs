﻿using SharedCode.Code.Models;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Interfejs klasy repozytorium dla obiektów typu ProjectRoomModel
    /// </summary>
    public interface IProjectRoomRepository : IRepositoryAsync<ProjectRoomModel>
    {
    }
}