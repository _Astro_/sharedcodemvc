﻿using System.Threading.Tasks;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Interfejs klasy UnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        IProjectRoomRepository ProjectRoomRepository { get; }

        Task Commit();

        void Dispose();        
    }
}