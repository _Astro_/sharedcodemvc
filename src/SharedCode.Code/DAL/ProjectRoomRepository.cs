﻿using SharedCode.Code.Models;
using System.Data.Entity;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Klasa repozytorium dla obiektów typu ProjectRoomModel
    /// </summary>
    public class ProjectRoomRepository : RepositoryAsync<ProjectRoomModel>, IProjectRoomRepository
    {
        public ProjectRoomRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}