﻿using System.Threading.Tasks;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Klasa UnitOfWork
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private AppDbContext _appDbContext;
        private IProjectRoomRepository _projectRoomRepository;

        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        public UnitOfWork()
        {
            _appDbContext = GetAppDbContext();
        }

        public IProjectRoomRepository ProjectRoomRepository
        {
            get
            {
                if (_projectRoomRepository == null)
                    _projectRoomRepository = new ProjectRoomRepository(_appDbContext);
                return _projectRoomRepository;
            }
        }

        /// <summary>
        /// Metoda asynchronicznego zapisu/zatwierdzenia zmian w bazie danych
        /// </summary>
        /// <returns></returns>
        public virtual async Task Commit()
        {
            await _appDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Metoda zwalniania obiektu UnitOfWork
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Metoda zwalniania zasobów klasy i samego obiektu UnitOfWork
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_appDbContext != null)
                    _appDbContext.Dispose();
            }
        }

        /// <summary>
        /// Metoda zwraca instancję kontekstu bazy danych
        /// </summary>
        /// <returns></returns>
        private AppDbContext GetAppDbContext()
        {
            return new AppDbContext();
        }
    }
}