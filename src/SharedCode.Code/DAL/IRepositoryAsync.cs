﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SharedCode.Code.DAL
{
    /// <summary>
    /// Interfejs generycznego repozytorium asynchronicznego
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepositoryAsync<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(string id);

        Task<IEnumerable<T>> GetByExpressionsAsync(Expression<Func<T, bool>> predicate);

        void AddAsync(T entity);

        void UpdateAsync(T entity);

        void Delete(T entity);

        Task<int> CountAsync();
    }
}